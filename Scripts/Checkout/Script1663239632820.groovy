import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import org.openqa.selenium.WebElement as WebElement

//Open Browser
WebUI.openBrowser('https://www.ruparupa.com/')

WebUI.maximizeWindow()

WebUI.waitForElementPresent(findTestObject('btn_rmhtangga'), 2)

WebUI.click(findTestObject('btn_rmhtangga'))

WebUI.waitForElementPresent(findTestObject('lbl_promo'), 2)

WebUI.scrollToElement(findTestObject('lbl_promo'), 3)

WebUI.click(findTestObject('checkbox_buy'))

WebUI.scrollToElement(findTestObject('lbl_urutberdasarkan'), 3)

WebUI.click(findTestObject('cmb_urut'))

WebUI.click(findTestObject('cmb_produkterbaru'))

def GetTitle = WebUI.getText(findTestObject('semua_product'))

//Get Produk baris pertama
def FixTitle = CustomKeywords.'function.logic.GetTitleProduct'(GetTitle)

WebUI.click(findTestObject('img_product',[('Fixtitle') : FixTitle]))

WebUI.waitForElementPresent(findTestObject('btn_infocicilan'), 2)

WebUI.click(findTestObject('btn_infocicilan'))

WebUI.waitForElementPresent(findTestObject('btn_tmbhkeranjang'), 2)

WebUI.click(findTestObject('btn_tmbhkeranjang'))

WebUI.waitForElementPresent(findTestObject('btn_tmbhkeranjang'), 2)

WebUI.click(findTestObject('btn_lnjutkeranjang'))

WebUI.waitForElementPresent(findTestObject('btn_lanjutpem'), 2)

WebUI.click(findTestObject('btn_lanjutpem'))

WebUI.waitForElementPresent(findTestObject('txt_email'), 2)

//email'
WebUI.setText(findTestObject('txt_email'), 'ekaferdyans@yopmail.com')

//Input password'
WebUI.setText(findTestObject('txt_password'), 'asalbosku')

WebUI.click(findTestObject('btn_masuk'))

WebUI.verifyTextPresent('Informasi tidak valid', false, FailureHandling.STOP_ON_FAILURE)

WebUI.closeBrowser();

