<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>img_product</name>
   <tag></tag>
   <elementGuidId>28e0fd02-7372-4e52-94ba-68883b75f3d2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//img[@id='image-primary' and @title='${Fixtitle}']</value>
      </entry>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
