<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_tmbhkeranjang</name>
   <tag></tag>
   <elementGuidId>fcfaac9d-74ad-4bb9-9c35-1792b86d55b5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@class='btn btn-primary btn-l btn-full' and text()='Tambah ke keranjang']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
