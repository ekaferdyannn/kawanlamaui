<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_lnjutkeranjang</name>
   <tag></tag>
   <elementGuidId>59794c1e-9096-446c-8b70-942e556324ba</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//button[@class='btn btn-primary btn-cart bold' and text()='Lanjut Ke Keranjang']</value>
      </entry>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
</WebElementEntity>
